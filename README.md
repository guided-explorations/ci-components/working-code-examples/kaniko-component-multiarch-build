# Kaniko Component Building Multiarch Containers

Demonstrates how to use the Kaniko Component to build Multiarch containers.

Examine the [pipeline jobs](https://gitlab.com/guided-explorations/ci-components/working-code-examples/kaniko-component-multiarch-build/-/pipelines) for container builds and test results.

Examine the [container registry](https://gitlab.com/guided-explorations/ci-components/working-code-examples/kaniko-component-multiarch-build/container_registry/6439776) to see the containers and tags.

## If This Helps You, Please Star The Original Source Project
One click can help us keep providing and improving this component.  If you find this information helpful, please click the star on this components original source here: [Project Details](https://gitlab.com/guided-explorations/ci-components/kaniko)